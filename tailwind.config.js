module.exports = {
  purge: ['./src/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        foodpanda: {
          DEFAULT: "#D70F64",
          50: "#FCD0E2",
          100: "#FAB8D4",
          200: "#F788B7",
          300: "#F3599A",
          400: "#F0297E",
          500: "#D70F64",
          600: "#A70C4E",
          700: "#780838",
          800: "#480521",
          900: "#18020B",
        },
        lineman: {
          DEFAULT: "#2DBE57",
          50: "#DAF6E2",
          100: "#C6F1D2",
          200: "#9CE8B2",
          300: "#73DE92",
          400: "#4AD472",
          500: "#2DBE57",
          600: "#239544",
          700: "#196C31",
          800: "#10421E",
          900: "#06190B",
        },
      },
    },
  },
  variants: {
    extend: {
      ringWidth: ["hover"],
      ringColor: ["hover"],

    },
  },
  plugins: [],
};
