export type Provider = "foodpanda" | "lineman" | "robinhood";

export interface UpdateData {
    name: string,
    desc: string,
    size: number,
    filename: string,
    date: string
  }
  
  export interface Menu {
    order: number
    name: string;
    type: FoodType;
    displayName: string;
    alternativeName: string[];
    enable: boolean;
    delivery: {
      price: number,
      extraPrice: number
    };
    price: number;
    extraPrice: number;
    fileUrl: string;
    selected: boolean;
    id: string;
    [Symbol.iterator](): IterableIterator<number>;
  }
  
  export interface Order {
    items: Array<Item>;
    total: number;
    gpTotal: number;
    timestamp: any;
    date?: string;
    user?: string;
    id: string;
    source: string;
    count: number;
    canEdit?: boolean;
    error?: boolean
    raw?: any;
  }
  
  export type FoodType = "pork" | "chicken"
  
  export interface Item {
    choices: Array<any>;
    data?: Menu;
    fid: string;
    sizes?: Array<any>;
    total: number;
  }
  