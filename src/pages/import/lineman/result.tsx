import { LineMANParser } from "../../../importer/lineman";
import { Fragment, useState } from "react";
import { useRouter } from "next/router";
import { FetcherTable, Header } from "../../../importer";
import Head from "next/head";
import Modal from "../../../modal";
import { AlertTriangle } from "react-feather";
import NavigationPrompt from "../../../navigationPrompt";
import axios from "axios";

import nookies from "nookies";
import admin from "../../../firebase-admin";

export const getServerSideProps = async (context) => {
  if (!context.query.id) {
    return {
      redirect: {
        destination: "/import",
        permanent: true,
      },
    };
  }
  try {
    const cookies = nookies.get(context);
    await admin.auth().verifySessionCookie(cookies._auth,true);
  }
  catch(err) {
    return {
      redirect: {
        destination: "/import/error?type=unauthorized",
      },
    };
  }
  try {
    let parser = new LineMANParser();
    await parser.readFile(context.query.id);
    return {
      props: {
        raw: parser.rawResult,
        parsed: await parser.constructOrder(),
        error: parser.errors,
        fileError: parser.fileError,
      },
    };
  } catch (err) {
    return {
      props: {
        error: true,
      },
    };
  }
};
export default function LMResult({ fileError, ...props }) {
  const router = useRouter();
  const confirmMsg = "Data hasn't imported yet. Exit this page?";
  const [confirmModal, setConfirm] = useState({
    show: false,
    start: false,
    error: false,
    success: false,
  });
  async function addItem() {
    if(confirmModal.start) return;
    setConfirm((state) => ({...state, start: true}));
    try {
      let fetch = await axios.post(process.env.NEXT_PUBLIC_API_ENDPOINT + "import/batch", new URLSearchParams({id: router.query.id.toString()}), {
        params: {
          source: "lineman"
        }
      });
      if(fetch.data.success) {
        setConfirm((state) => ({...state, success: true}));
        sessionStorage.setItem("showSuccess", "true");
        router.replace("/import?success=true")
      }
    }
    catch(err) {
      setConfirm((state) => ({...state, error: true, show: false}));
    }
  }
  function confirmExit() {
    if(confirm(confirmMsg)) {
      setConfirm((state) => ({...state, success: true}));
      router.back();
    }
  }
  return (
    <div className="md:p-10 p-6">
      <Head>
        <title>Results: LINE MAN Import</title>
      </Head>
      <Header type="lineman" showBack={true} onBack={() => confirmExit()}/>
      {!fileError && props.parsed ? (
        <Fragment>
          <main className="p-5">
            <h1 className="font-bold text-2xl">Results</h1>
            <div className="flex flex-row">
              <div className="pt-5 flex-grow">
                <span className="font-medium text-gray-600">
                  {props.parsed.length} order
                  {props.parsed.length > 1 ? "s" : ""} processed.
                </span>
                <br />
                <span className="font-medium text-red-500">
                  {props.error.length} order{props.error.length > 1 ? "s" : ""}{" "}
                  failed.
                </span>
              </div>
              <div className="flex flex-row-reverse">
                <button
                  onClick={() =>
                    setConfirm({ show: true, start: false, error: false, success:false })
                  }
                  className="bg-lineman-500 hover:bg-lineman-600 focus:outline-none focus:ring-2 focus:ring-offset-white focus:ring-offset-2 focus:ring-lineman-500 rounded-lg text-white font-semibold text-lg px-8 my-2"
                >
                  Add
                </button>
                {confirmModal.error && (
                  <span className="px-4 py-2 text-red-500 text-sm"><br/>Cannot import, try again.</span>
                )}
              </div>
            </div>
          </main>

          <FetcherTable {...props} />
          <Modal show={confirmModal.show}>
            <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
              <div className="sm:flex sm:items-start">
                <div className="mx-auto flex-shrink-0 flex items-center justify-center h-12 w-12 rounded-full bg-green-100 sm:mx-0 sm:h-10 sm:w-10">
                  <AlertTriangle color="#059669" size={20} />
                </div>
                <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                  <h3
                    className="text-lg leading-6 font-medium text-gray-900"
                    id="modal-headline"
                  >
                    Add data
                  </h3>
                  <div className="mt-2">
                    <div className="text-gray-500">
                      Confirm adding {props.parsed.length} item
                      {props.parsed.length > 1 ? "s" : ""} to the database?{" "}
                      <br />
                      <p className="text-sm text-red-500 py-4">
                        Any orders with existing IDs will be replaced without
                        notice.
                        <br />
                        Any failed orders will be skipped automatically.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
              <button
                onClick={addItem}
                type="button"
                className={(!confirmModal.start ? "bg-green-500 hover:bg-green-600" : "bg-green-400 cursor-not-allowed") + " focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500 w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 text-base font-medium text-white sm:ml-3 sm:w-auto sm:text-sm"}
              >
                Confirm
              </button>
              <button
                onClick={() =>
                  setConfirm({ show: false, error: false, start: false, success: false })
                }
                type="button"
                className="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-400 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
              >
                Cancel
              </button>
            </div>
          </Modal>
          <NavigationPrompt when={confirmModal.success} message={confirmMsg} />
        </Fragment>
      ) : (
        <main className="p-5">
          <h1 className="font-bold text-red-500 text-2xl">
            Could not import the current file
          </h1>
          <p className="py-5">
            This may due to a corrupted operation while auto-import, or the
            source file is invalid.
            <br />
            Please try again or use "Manual Import". If the problem still
            persisted, contact the administrator.
          </p>
        </main>
      )}
    </div>
  );
}
