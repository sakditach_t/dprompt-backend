import Head from "next/head";
import { useState, useRef } from "react";
import { Header, FetcherComponent } from "../../../importer";
import Image from "next/image";
import { useRouter } from "next/router";

import nookies from "nookies";
import admin from "../../../firebase-admin";
export const getServerSideProps = async function (context) {
  const cookies = nookies.get(context);
  // Check if token exists
  try {
    await admin.auth().verifySessionCookie(cookies._auth,true);
    return {
      props: {},
    };
  }
  catch(err) {
    return {
      redirect: {
        destination: "/import/error?type=unauthorized",
      },
    };
  }
};

export default function LMImportMail(props) {
  const router = useRouter();
  const fetcher = useRef(null);
  const fileUpload = useRef<HTMLInputElement>(null);
  const [autoImport, startImport] = useState(false);
  const [backBtn, showBack] = useState(true);

  function finalize(id) {
    if (!id) return showBack(true);
    router.push({
      pathname: "/import/lineman/result",
      query: { id },
    });
  }

  function importData() {
    startImport(true);
    fetcher.current.setShow(true);
    showBack(false);
    fetcher.current.importData();
  }
  function goBack() {
    if (autoImport) {
      startImport(false);
      fetcher.current.setShow(false);
    } else if (fetcher.current.warned()) {
      router.replace("/import");
    } else {
      router.back();
    }
  }
  function manualUpload(e) {
    startImport(true);
    fetcher.current.setShow(true);
    showBack(false);
    fetcher.current.manualUpload(e.target.files[0]);    
  }
  return (
    <div className="md:p-10 p-6">
      <Head>
        <title>Mail: LINE MAN Import</title>
      </Head>
      <Header type="lineman" showBack={backBtn} onBack={() => goBack()} />
      {!autoImport && (
        <main className="p-5">
          <p className="text-lineman-600 font-medium">
            This provider supports auto-import.
          </p>
          <h2 className="font-bold text-xl py-4">Import Instuctions</h2>
          <hr />
          <div className="grid pt-3 md:leading-7 leading-9">
            <ol className="list-decimal px-6">
              <li>
                Open Wongnai Merchant Application and select <b>"Order history/ประวัติการสั่งซื้อ"</b>.
              </li>
              <li>Select range to import by clicking on the date box.</li>
              <li>
                Press on the export icon. &nbsp;
                <Image src="/share.svg" height={20} width={20} />
              </li>
              <li>A prompt will shown, press <b>"Send email/ส่งอีเมล"</b>.</li>
              <li>Proceed back to this page and click <b>"Auto import"</b>.</li>
            </ol>
          </div>
          <div className="grid grid-cols-2 p-5 gap-4">
            <button
              onClick={() => importData()}
              className="p-2 rounded text-white bg-green-500 hover:bg-green-600 font-bold"
            >
              Auto import
            </button>
            <button 
            onClick={() => fileUpload.current.click()}
            className="p-2 rounded border border-red-500 hover:bg-red-500 hover:text-white">
              Manual import <br />
              (Upload file)
            </button>
            <input className="hidden" onChange={manualUpload} type="file" accept=".csv" ref={fileUpload} />
          </div>
        </main>
      )}
      <FetcherComponent
        ref={fetcher}
        source="lineman"
        callback={(d) => finalize(d)}
      />
    </div>
  );
}
