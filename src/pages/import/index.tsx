import Head from "next/head";
import Link from "next/link";
import {
  capitalize,
  redirectHeader,
  checkAuthCookie,
  getMenuLastModified,
} from "../../util";
import { Dialog, Transition } from "@headlessui/react";
import { Fragment, useEffect, useRef, useState } from "react";
import React from "react";
import Image from "next/image";
import { promises as fs } from "fs";
import axios from "axios";

import nookies from "nookies";
import admin from "../../firebase-admin";
export const getServerSideProps = async function (context) {
  const cookies = nookies.get(context);
  // Check if token exists
  try {
    await admin.auth().verifySessionCookie(cookies._auth,true);
    return {
      props: {
        time: await getMenuLastModified(fs),
        success: context.query.success ? true : false
      },
    };
  }
  catch(err) {
    console.log(err);
    return redirectHeader();
  }
};

export default function ImportPage(props) {
  const [error, setError] = useState(false);
  const [time, setTime] = useState(props.time);
  const [open, setOpen] = useState(false);
  const cancelButtonRef = useRef();
  async function updateDB() {
    if (error) return;
    try {
      let fetch = await axios.get(
        process.env.NEXT_PUBLIC_API_ENDPOINT + "import/updateMenu"
      );
      if (fetch.data.success) {
        return setTime(fetch.data.time);
      }
      setError(true);
    } catch (err) {
      setError(true);
    }
  }
  useEffect(() => {
    if (sessionStorage.getItem("showSuccess")) {
      setTimeout(() => {
        setOpen(true);
        sessionStorage.removeItem("showSuccess");
      }, 500);
    }
  })

  return (
    <div className="flex flex-col items-center justify-center min-h-screen py-2">
      <Head>
        <title>D-PromptAdmin Importer</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      {props.success && (
         <Transition.Root show={open} as={Fragment}>
         <Dialog
           as="div"
           static
           className="fixed z-10 inset-0 overflow-y-auto"
           initialFocus={cancelButtonRef}
           open={open}
           onClose={() => setOpen(false)}
         >
           <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
             <Transition.Child
               as={Fragment}
               enter="ease-out duration-300"
               enterFrom="opacity-0"
               enterTo="opacity-100"
               leave="ease-in duration-200"
               leaveFrom="opacity-100"
               leaveTo="opacity-0"
             >
               <Dialog.Overlay className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
             </Transition.Child>
   
             {/* This element is to trick the browser into centering the modal contents. */}
             <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">
               &#8203;
             </span>
             <Transition.Child
               as={Fragment}
               enter="ease-out duration-300"
               enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
               enterTo="opacity-100 translate-y-0 sm:scale-100"
               leave="ease-in duration-200"
               leaveFrom="opacity-100 translate-y-0 sm:scale-100"
               leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
             >
                <div className="inline-block w-full max-w-lg p-6 my-8 overflow-hidden text-left align-middle transition-all transform bg-white shadow-xl rounded-2xl">
              <Dialog.Title
                as="h3"
                className="text-lg font-medium leading-6 text-gray-900"
              >
                Data imported successfully.
              </Dialog.Title>
              <div className="mt-4">
                <p className="text-sm text-gray-500">
                  If anything goes wrong, contact admin within 24 hours to make changes. Otherwise the process can't be undone!
                </p>
              </div>

              <div className="mt-4">
                <button
                  type="button"
                  className="inline-flex justify-center px-4 py-2 text-sm font-medium text-blue-900 bg-blue-100 border border-transparent rounded-md hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 focus-visible:ring-blue-500"
                  onClick={() => setOpen(false)}
                >
                  Got it, thanks!
                </button>
              </div>
            </div>
             </Transition.Child>
           </div>
         </Dialog>
       </Transition.Root>
      )}
      <main className="flex flex-col items-center justify-center flex-1 px-20 text-center mb-10">
        <Image src="/logo.png" height={128} width={128} />

        <p className="md:text-3xl text-xl font-medium">
          External Data Importer
        </p>

        <p className="my-6 text-gray-500">
          To begin, select your target provider.
        </p>
        <div className="flex flex-wrap items-center gap-4 justify-around max-w-4xl sm:w-full">
          <Link href="/import/lineman" passHref={true}>
            <a className="p-6 mt-6 text-center border w-64 rounded-xl hover:text-gray-100 focus:text-gray-400 hover:shadow-lg bg-lineman text-white">
              <Image src="/lineman.png" width={64} height={64} className="" />
              <h3 className="text-2xl font-bold">LINE MAN</h3>
            </a>
          </Link>
          <Link href="/import/foodpanda" passHref={true}>
            <a className="p-6 mt-6 text-center border w-64 rounded-xl hover:text-gray-100 focus:text-gray-400 hover:shadow-lg bg-foodpanda text-white">
              <Image
                src="/foodpanda.webp"
                width={64}
                height={64}
                className=""
              />
              <h3 className="text-2xl font-bold">Foodpanda</h3>
            </a>
          </Link>
          {false && (
            <a
              href="https://nextjs.org/docs"
              className="robinhood-bg p-6 mt-6 text-center border w-64 rounded-xl hover:text-gray-800 focus:text-gray-400 hover:shadow-lg"
            >
              <Image src="/robinhood.png" width={64} height={64} className="" />
              <h3 className="text-2xl font-bold">Robinhood</h3>
            </a>
          )}
        </div>
        <div className="pt-5 text-sm text-gray-500">
          Menu DB last updated at {time}
          <br />
          <button
            className={
              "font-bold focus:outline-none " +
              (error ? "text-red-500 cursor-not-allowed" : "underline")
            }
            onClick={() => updateDB()}
          >
            {!error ? "Update now" : "Cannot update, please reload."}
          </button>
        </div>
      </main>

      <footer className="flex items-center flex-col justify-center w-full h-24 border-t">
        <span className="font-bold text-lg text-red-500">
          Internal Use Only - Version 0.1-beta
        </span>
        <span className="text-sm mt-2 text-gray-500">
          Copyright &copy; {new Date().getFullYear()} Lemasc Service Co., Ltd
        </span>
      </footer>
    </div>
  );
}
