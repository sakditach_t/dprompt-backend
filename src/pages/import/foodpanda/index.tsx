import Head from "next/head";
import { useState, useRef } from "react";
import { DatePicker } from "../../../Datepicker";
import dayjs from "dayjs";
import { Header, FetcherComponent }from "../../../importer";
import { useRouter } from "next/router";

import nookies from "nookies";
import admin from "../../../firebase-admin";
export const getServerSideProps = async function (context) {
  const cookies = nookies.get(context);
  // Check if token exists
  try {
    await admin.auth().verifySessionCookie(cookies._auth,true);
    return {
      props: {},
    };
  }
  catch(err) {
    return {
      redirect: {
        destination: "/import/error?type=unauthorized",
      },
    };
  }
};

export default function FPImportMail(props) {
  const router = useRouter();
  const fetcher = useRef(null);
  const fileUpload = useRef<HTMLInputElement>(null);
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  const [autoImport, startImport] = useState(false);
  const [backBtn, showBack] = useState(true);

  function finalize(id) {
    if (!id) return showBack(true);
    router.push({
      pathname: "/import/foodpanda/result",
      query: { id },
    });
  }

  function importData() {
    startImport(true);
    fetcher.current.setShow(true);
    showBack(false);
    fetcher.current.importData(
      new URLSearchParams({
        from: dayjs(startDate).format("YYYY-MM-DD"),
        to: dayjs(endDate).format("YYYY-MM-DD"),
      })
    )
  }
  function goBack() {
    if (autoImport) {
      startImport(false);
      fetcher.current.setShow(false);
    } else if(fetcher.current.warned()) {
      router.replace("/import");
    }
    else {
      router.back();
    }
  }
  function manualUpload(e) {
    startImport(true);
    fetcher.current.setShow(true);
    showBack(false);
    fetcher.current.manualUpload(e.target.files[0]);    
  }

  return (
    <div className="md:p-10 p-6">
      <Head>
        <title>Mail: Foodpanda Import</title>
      </Head>
      <Header type="foodpanda" showBack={backBtn} onBack={() => goBack()} />
      {!autoImport && (
        <main className="p-5">
          <p className="text-foodpanda-400 font-medium">
            This provider supports auto-fetch and auto-import.
          </p>
          <h2 className="font-bold text-xl py-4">Import Range</h2>
          <hr />
          <div className="grid pt-3">
            <div className="grid md:grid-cols-2 py-2">
              <span>Start Date: </span>
              <DatePicker
                config={{ date: startDate, maxDate: endDate }}
                onChange={setStartDate}
              ></DatePicker>
            </div>
            <div className="grid md:grid-cols-2 py-2 ">
              <span>End Date: </span>
              <DatePicker
                config={{ date: endDate, maxDate: new Date() }}
                onChange={setEndDate}
              ></DatePicker>
            </div>
          </div>
          <div className="grid grid-cols-2 p-5 gap-4">
            <button
              onClick={() => importData()}
              className="p-2 rounded text-white bg-green-500 hover:bg-green-600 font-bold"
            >
              Auto import
            </button>
            <button
             onClick={() => fileUpload.current.click()}
              className="p-2 rounded border border-red-500 hover:bg-red-500 hover:text-white">
              Manual import <br />
              (Upload file)
            </button>
            <input className="hidden" onChange={manualUpload} type="file" accept=".csv" ref={fileUpload} />
          
          </div>
        </main>
      )}
      <FetcherComponent
        ref={fetcher}
        source="foodpanda"
        callback={(d) => finalize(d)}
      />
    </div>
  );
  //
}
