import Image from "next/image";
import Head from "next/head"
import { useRouter } from "next/router";
export default function Error(props) {
    const router = useRouter()
    let code;
    switch(router.query.type) {
        case "unauthorized":
            code = "Please login before continue (CODE: UNAUTHORIZED)";
            break;
        case "relogin":
            code = "Session expired, please relogin (CODE: RELOGIN)"
            break;
        default:
            code = "CODE: INTERNAL"
    }
  return (
    <div className="flex flex-col items-center justify-center min-h-screen py-2">
    <Head>
        <title>Error: D-PromptServe</title>
    </Head>
      <main className="flex flex-col items-center justify-center flex-1 px-20 text-center mb-10">
      <Image src="/logo.png" height={128} width={128} />
        <h1 className="md:text-3xl text-xl font-bold">Error</h1>
        <div className="p-4">Could not proceed the current request on this server.</div>
        <div className="text-gray-500 text-sm">{code}</div>
      </main>
    </div>
  );
}
