import type { NextApiRequest, NextApiResponse } from 'next'
import {FoodpandaFetcher} from "../../../importer/foodpanda"
import {promises as fs} from "fs";
import path from "path";
import { ParsedMail } from 'mailparser';
import { withCookie } from '../../../withSession';
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
/**
 * Foodpanda Attachment download
 * 
 * After mail has been saved, read data in chronological order, sort by latest date.
 * Extract download token and attempt to download the report file.
 */
interface SavedMail {
    id: number;
    data: ParsedMail
}
const FoodpandaDownload = async function (req: NextApiRequest, res: NextApiResponse) {
    if(req.method != "POST" || !req.body.id) {
        return res.status(400).json({success: false})
    }
    try {
        const foodpanda = new FoodpandaFetcher(process.env.IMPORT_EMAIL,process.env.FOODPANDA_PASS);
        await foodpanda.login();
        let data = await fs.readFile(path.join(process.cwd(),"mail","foodpanda",req.body.id + ".json"),{
            encoding: "utf-8"
        });
        let results:SavedMail[] = JSON.parse(data);
        results = results.reverse();
        for(const result of results) {
            await sleep(1000);
            const text = result.data.text;
            const url = new URL(text.substring(text.search("https://"),text.search("Note:")));
            const token = url.pathname.split("/").pop();
            const time = new Date().valueOf();
            if(await foodpanda.downloadReports(token,path.join(process.cwd(),"mail","foodpanda",time + ".csv"))) {
                await foodpanda.logout();
                return res.status(200).json({success: true, data: time});
            }
        };
        await foodpanda.logout();
        return res.status(400).json({success: false});
    }
    catch(err) {
        console.error(err);
        return res.status(400).json({success: false});
    }
}
export default withCookie(FoodpandaDownload);