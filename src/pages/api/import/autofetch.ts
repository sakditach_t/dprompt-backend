import type { NextApiRequest, NextApiResponse } from 'next'
import { FoodpandaFetcher } from "../../../importer/foodpanda"
import { withCookie } from '../../../withSession';

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export default withCookie(async (req: NextApiRequest, res: NextApiResponse) => {
    if (req.method != "POST" || !req.query.source || req.query.source != "foodpanda") {
        res.status(400).json({ success: false })
    }
    if (!req.body.from || !req.body.to) return res.status(400).json({ success: false })
    try {
        const foodpanda = new FoodpandaFetcher(process.env.IMPORT_EMAIL, process.env.FOODPANDA_PASS);
        await foodpanda.login();
        await sleep(2000);
        let report = await foodpanda.getReports(req.body.from, req.body.to);
        await foodpanda.logout()
        if (report) {
            return res.status(200).send({ success: true });
        } else {
            throw new Error("Cannot getReports")
        }
    }
    catch (err) {
        console.error(err);
        return res.status(500).send({ success: false })
    }
})
