import type { NextApiRequest, NextApiResponse } from 'next'
import {promises as fs} from "fs";
import path from "path";
export default async (req: NextApiRequest, res: NextApiResponse) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    const authHeader = req.headers.authorization;
    if (!authHeader || req.method != "GET") {
      return res.status(401).json({ success: false });
    }
    const token = authHeader.split(" ")[1];
    if(token !== process.env.CRON_KEY) {
        return res.status(401).json({ success: false });
    }
    try {
        const mailPath = path.join(process.cwd(),"mail")
        let dirs = await fs.readdir(mailPath);
        let fileCount = 0;
        await Promise.all(dirs.map(async (d) => {
            console.log(d);
            let files = await fs.readdir(path.join(mailPath,d));
            await Promise.all(files.map(async (f) => {
                if(f == ".gitignore") return null;
                let stat = await fs.stat(path.join(mailPath,d,f));
                let now = new Date().getTime();
                let endTime = stat.mtime.getTime() + (24 * 60 * 60 * 1000);
                if(now > endTime) {
                    fileCount++;
                    await fs.unlink(path.join(mailPath,d,f));
                }
            }))
        }));
        return res.status(200).json({ success: true, files: fileCount });
        
    }
    catch(err) {
        console.log(err);
    }
};