
import { withAuth } from "../../../withSession";
import type { NextApiRequest, NextApiResponse } from 'next'
import nookies from "nookies"
import admin from "../../../firebase-admin";
import path from "path";
import { createLogger, format, transports } from 'winston';
const { combine, timestamp, label, prettyPrint, printf } = format;

const myFormat = printf(({ level, message, label, timestamp }) => {
  return `${timestamp} [${label}] ${level}: ${message}`;
});

const logger = createLogger({
  level: 'info',
  format: combine(
    timestamp(),
    label({ label: 'session' }),
    prettyPrint(),
    myFormat
  ),
  transports: [
    new transports.File({ filename: path.join(process.cwd(),'server.log') }),
  ],
});


export default async (req: NextApiRequest, res: NextApiResponse) => {
    // Inapp Browser doesn't support POST method
    // So we need to implement this by hand.
    var ip = req.headers['x-real-ip'] || req.socket.remoteAddress
      if (ip && ip.toString().substr(0, 7) == "::ffff:") {
        ip = ip.toString().substr(7)
        console.log(ip)
      }
    logger.info("Session cookie for user IP: "+ ip);
    logger.info("User-agent: " + req.headers["user-agent"]);
    const token = req.query.token;
    if (!token) {
        logger.error("User don't have token; redirect");
        return res.redirect("/import/error?type=unauthorized");
    }
    try {
        logger.info("Verifying ID token..");
        let decodedIdToken = await admin.auth().verifyIdToken(token.toString());
        if (new Date().getTime() / 1000 - decodedIdToken.auth_time > 15 * 60) {
            logger.error("Token expired; redirect");
            return res.redirect("/import/error?type=relogin")
        }
        const expiresIn = 15 * 60 * 1000;
        // Create session cookie and set it.
        const savedCookies = nookies.get({req});
        if(savedCookies._auth) {
            nookies.destroy({res},"._auth");
            console.log("Cleared");
        }
        const cookie = await admin.auth().createSessionCookie(token.toString(), { expiresIn });
        nookies.set({ res }, "_auth", cookie, {
            //expiresIn,
            path: "/",
            httpOnly: true,
            overwrite: true
        });
        logger.info("Session OK; redirect to index.")
       res.redirect("/import");
    } catch (err) {
        logger.info("Session Error:");
        logger.info(JSON.stringify(err));
        return res.redirect("/import/error")
    }
};