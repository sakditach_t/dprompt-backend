import type { NextApiRequest, NextApiResponse } from 'next'
import nookies from "nookies"
import admin from "../../../firebase-admin";
import { withCookie } from "../../../withSession";
import { promises as fs } from "fs";
import path from "path";
import { getMenuLastModified } from '../../../util';

export default withCookie(async (req: NextApiRequest, res: NextApiResponse) => {
    try {
        const db = admin.firestore();
        const data = await db.collection("menus").get();
        let result = data.docs.map(d => ({ id: d.id, ...d.data() }));
        await fs.writeFile(path.join(process.cwd(), "menu.json"), JSON.stringify(result));
        return res.status(200).json({ success: true, time: await getMenuLastModified(fs) });
    }
    catch (err) {
        console.log(err);
        res.status(500).json({ success: false })
    }
});