import type { NextApiRequest, NextApiResponse } from 'next'
import nookies from "nookies"
import admin from "../../../firebase-admin";
import { promises as fs } from "fs";
import path from "path";
import multer from 'multer';
import nextConnect from "next-connect";
import { withCookie } from '../../../withSession';
console.clear();
const timestamp = new Date().valueOf().toString();
const upload = multer({
  storage: multer.diskStorage({
    destination: path.join(process.cwd(),'mail','anonymous'),
    filename: (req, file, cb) => cb(null, timestamp + ".csv"),
  }),
});

const apiRoute = nextConnect<NextApiRequest, NextApiResponse>({
  // Handle any other HTTP method
  onNoMatch(req, res) {
    res.status(405).json({ success: false });
  },
  onError(err, req, res, next) {
    console.log(err);
    res.status(500).json({success: false});
  }
});

apiRoute.use(upload.single('csv'));

apiRoute.post((req, res) => {
  res.status(200).json({ success: true, data: timestamp });
});

export default withCookie(apiRoute);

export const config = {
  api: {
    bodyParser: false, // Disallow body parsing, consume as stream
  },
};