import type { NextApiRequest, NextApiResponse } from 'next'
import {promises as fs} from "fs";
import path from "path";
import admin from "../../../firebase-admin";
import { Order } from '../../../types';
import dayjs from "dayjs";

import { withCookie } from '../../../withSession';
import { createLogger, format, transports } from 'winston';
const { combine, timestamp, label, prettyPrint, printf } = format;

const enumerateErrorFormat = format(info => {
  // @ts-expect-error: Error instance!
  if (info.message instanceof Error) {
    info.message = Object.assign({
      message: info.message.message,
      stack: info.message.stack
    }, info.message);
  }

  if (info instanceof Error) {
    return Object.assign({
      message: info.message,
      stack: info.stack
    }, info);
  }

  return info;
});

const logger = createLogger({
level: 'info',
format: combine(
  timestamp(),
  label({ label: 'foodpandaFetcher' }),
  enumerateErrorFormat(),
  format.json()
),
transports: [
  new transports.File({ filename: path.join(process.cwd(),'server.log') }),
],
});

export default withCookie(async (req: NextApiRequest, res: NextApiResponse) => {
    if (req.method != "POST" || !req.body.id || !req.query.source) {
        return res.status(400).json({ success: false })
    }
    try {
        logger.info("Filename " + req.body.id);
        logger.info("Source: " + req.query.source);
        const data:Order[] = JSON.parse(await fs.readFile(path.join(process.cwd(),"mail",req.query.source.toString(),req.body.id + "-parsed.json"),{
            encoding: "utf-8"
        }));
        const db = admin.firestore();
        const batch = db.batch();
        data.map((v) => {
            if(v.error) return null;
            let order = Object.assign({},v);
            delete order.id;
            delete order.raw;
            delete order.error;
            order.items = order.items.map(i => {
                delete i.data;
                return i;
            })
            order.timestamp = dayjs(v.timestamp).toDate();
            batch.set(db.collection("orders").doc(dayjs(v.timestamp).format("YYYY-MM-DD")).collection("details").doc(v.id), order);
        });
        await batch.commit();
        logger.info("Batch update success.")
        return res.status(200).json({ success: true})
    }
    catch (err) {
        logger.error("Batch update failed.");
        logger.log({level: 'error', message: err});;
        console.error(err);
        return res.status(500).send({ success: false })
    }
});
