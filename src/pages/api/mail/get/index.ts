import type { NextApiRequest, NextApiResponse } from 'next'
import MailParser from "../../../../importer/mailParser";
import { promises as fs } from "fs";
import path from "path"
import dayjs from "dayjs"

/**
 * 2. Fetch attachments for each ID.
 * Normally only one or two mails (or even none) attachments are need to fetch.
 * We will skip pulling if file already exists at the destination
 */
 export default async (req: NextApiRequest, res: NextApiResponse) => {
    console.log("----- " + (new Date).toISOString() + ": Start Request");
    const mailClient = new MailParser(true);
    mailClient.client.once("ready", async () => {
        try {
            await mailClient.openInbox();
         /*   let filter;
            switch (req.query.source) {
                case "robinhood":
                    filter = [["FROM", "no-reply-robinhoodshop@robinhood.in.th"],
                    ["FROM", "edocument@mail.etaxsolution.com"], ["SINCE", "Apr 9, 2021"]];
                    break;
                case "foodpanda":
                    filter = [["FROM", "laps@scb.co.th"], ["FROM", "noreply@portal.restaurant-partners.com"], ["SINCE", "Apr 9, 2021"]]
                    break;
                case "lineman":
                    filter = [["FROM", "onesmile.sodsai@gmail.com"], ["FROM", "no-reply-merchant@lmwn.com"], ["SINCE", "Apr 9, 2021"]]
            }
            let s = await mailClient.searchMail(filter)*/
            let f = mailClient.client.fetch(319145,{
                bodies: ''
            })
            /* let f = mailClient.client.fetch(s,{
                 bodies: ["HEADER.FIELDS (FROM TO SUBJECT DATE)"]
             })*/
             let result;
             f.on('message',(msg, seqno) => mailClient.messageHandler(msg, seqno));

             f.once('error', function (err) {
                 console.log('Fetch error: ' + err);
             });
             f.once('end', function () {
                 console.log('Done fetching all messages!');
                 mailClient.client.end();
                 res.status(200).json(result);
             });
            /*const fileName = path.join(process.cwd(), "mail", req.query.source + dayjs().format("_YYYY-MM-DDTHHmmss.j[s]on"));
            await fs.writeFile(fileName, JSON.stringify(s))*/
        }
        catch (err) {
            console.error(err);
            res.status(500).json({ success: false })
            mailClient.client.end();
        }
    })
    mailClient.client.connect();
}