import type { NextApiRequest, NextApiResponse } from 'next'
import MailParser from "../../../importer/mailParser";
import { promises as fs } from "fs";
import path from "path"
import dayjs from "dayjs"

/**
 * 1. Fetch updated mail IDs.
 * 
 * This script will connect to the mail server and saves any mail ids 
 * matched with the source name provided.
 * Results will be saved to temp location.
 */

export default async (req: NextApiRequest, res: NextApiResponse) => {
    console.log("----- " + (new Date).toISOString() + ": Start Request");
    const mailClient = new MailParser(true);
    mailClient.client.once("ready", async () => {
        try {
            await mailClient.openInbox();
            let filter;
            switch (req.query.source) {
                case "robinhood":
                    filter = [["FROM", "no-reply-robinhoodshop@robinhood.in.th"],
                    ["FROM", "edocument@mail.etaxsolution.com"], ["SINCE", "Apr 9, 2021"]];
                    break;
                case "foodpanda":
                    //["FROM", "laps@scb.co.th"], 
                    filter = [["FROM", "noreply@portal.restaurant-partners.com"], ["SINCE", "Apr 9, 2021"]]
                    break;
                case "lineman":
                    filter = [["FROM", "onesmile.sodsai@gmail.com"], ["FROM", "no-reply-merchant@lmwn.com"], ["SINCE", "Apr 9, 2021"]]
            }
             let s = await mailClient.searchMail(filter);
             if(s.length == 0) return res.status(200).json({success: true})
              let f = mailClient.client.fetch(s,{
                   bodies: ""
               })
        /*    let f = mailClient.fetchMail({
                mode: "desc",
                pos: 1,
                count: 5,
            })
*/
            f.on('message', (msg, seqno) => mailClient.messageHandler(msg, seqno));

            f.once('error', function (err) {
                console.log('Fetch error: ' + err);
                throw err;
            });
            f.once('end', async function () {
                console.log('Done fetching all messages!');
                try {
                    res.status(200).json(await mailClient.parseQueue(s,req.query.source.toString()));
                }
                catch(err) {
                    throw err;
                }
            });
        }
        catch (err) {
            console.error(err);
            res.status(500).json({ success: false })
        }
        finally {
            mailClient.client.end();
        }
    })
    mailClient.client.connect();
}