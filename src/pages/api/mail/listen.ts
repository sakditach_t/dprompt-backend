import type { NextApiRequest, NextApiResponse } from 'next'
import dayjs from "dayjs"
import { promises as fs } from "fs";
import path from "path";
import MailClient from '../../../importer/mailParser';
import utc from "dayjs/plugin/utc"
import { withCookie } from '../../../withSession';
dayjs.extend(utc)

/**
 * Listen to mailbox changes.
 * 
 * - Client will fetch periodically to this page.
 * - This page will fetch server async for any recent mail changes.
 * - If server matches none, return false to client and let them fetch again.
 * - If server matches some results, return true to client and continue the process.
 * 
 */

const mailListener = async function (req: NextApiRequest, res: NextApiResponse) {
    if (!req.query.source) return res.status(400).json({ success: false })
    const mailClient = new MailClient(false);
    const timeSearch = dayjs().subtract(30, "minutes");
    const source = req.query.source.toString();
    mailClient.client.once("ready", async () => {
        try {
            await mailClient.openInbox();
            let filter = mailClient.getRecipents(source, timeSearch.utc().format("MMM D, YYYY"));
            let s = await mailClient.searchMail(filter);
            if (s.length == 0) return res.status(200).json({ success: false })
            let f = mailClient.client.fetch(s, {
                bodies: ""
            })
            f.on('message', (msg, seqno) => mailClient.messageHandler(msg, seqno));

            f.once('error', function (err) {
                console.log('Fetch error: ' + err);
                throw err;
            });
            f.once('end', async function () {
                console.log('Done fetching all messages!');
                try {
                    let queue = await mailClient.parseQueue(s, source);
                    queue = queue.filter(v => dayjs(v.data.date) > timeSearch);
                    if (queue.length == 0) {
                        return res.status(200).json({ success: false });
                    }
                    const fileTime = new Date().valueOf();
                    // Write data to local filesystem and returns the reference timestamp.
                    await fs.writeFile(path.join(process.cwd(), "mail", source, fileTime + ".json"), JSON.stringify(queue));
                    res.status(200).json({ success: true, data: fileTime});
                }
                catch (err) {
                    throw err;
                }
            });
        }
        catch (err) {
            console.error(err);
            res.status(500).json({ success: false })
        }
        finally {
            mailClient.client.end();
        }
    })
    mailClient.client.on("error",(err) => {
        console.log(err);
        res.status(500).json({ success: false })
    })
    mailClient.client.connect();
}
export default withCookie(mailListener);