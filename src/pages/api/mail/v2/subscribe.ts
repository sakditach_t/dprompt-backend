import type { NextApiRequest, NextApiResponse } from 'next'
import admin from "../.././../../firebase-admin"
export default async (req: NextApiRequest, res: NextApiResponse) => {
    res.setHeader("Access-Control-Allow-Origin","*");
    if (req.method != "POST") {
        return res.status(400).json({ success: false});
    }
    if(req.body.subscription != "projects/dpromptserve-vrubpf/subscriptions/maillistener-sub") {
        return res.status(400).json({ success: false});
    }
    try {
        let data = JSON.parse(Buffer.from(req.body.message.data, 'base64').toString());
        data.timestamp = new Date();
        await admin.firestore().collection("mail").doc().set(data);
        res.status(200).json({success: true});

    }
    catch(err) {
        console.log(err);
        return res.status(400).json({ success: false});
    }
}