import type { NextApiRequest, NextApiResponse } from 'next'
import { Octokit } from "@octokit/core";
import crypto from "crypto"
import fetch from "node-fetch"
import { withAuth } from '../../../withSession';

/**
 * Streams the latest release from Github and generate the random filename.
 */

export default withAuth(async (req: NextApiRequest, res: NextApiResponse) => {
    res.setHeader("Access-Control-Allow-Origin","*");
    if (req.method != "GET" || !req.query.token) {
        return res.status(400).json({ success: false});
    }
    const octokit = new Octokit({ auth: process.env.GITHUB_API_KEY });
    try {
        const latest = await octokit.request("/repos/{owner}/{repo}/releases/latest", {
            owner: process.env.GITHUB_OWNER,
            repo: process.env.GITHUB_REPO
        });
        const assets = latest.data.assets[0];
        let updateToken = crypto.createHash("sha256").update(process.env.APK_STARTKEY + assets.id + process.env.APK_ENDKEY).digest("hex");
        if (updateToken !== req.query.token) {
            throw new Error("Invalid download token");
        }

        /**
         * BUG: Vercel Serverless functions have their own timeout.
         * We need to serve from Github AWS server instead.
         * However,  Github generates their signature on-the-fly and we couldn't override anything else.
         * 
         * That's OK. We just don't want clients to see where our repo is :)
         * Carefully check the filename before uploading to Github Releases!
         */

        let remote = await fetch(assets.url, {
            headers: {
                "Accept": "application/octet-stream",
                "Authorization": `Bearer ${process.env.GITHUB_API_KEY}`,
            },
        });
        return res.status(302).redirect(remote.url);
        /*
        let filename = "v" + latest.data.tag_name + "." + crypto.randomBytes(12).toString("hex") + ".apk";
        res.setHeader("Content-Transfer-Encoding","Binary");
        res.setHeader('Content-disposition', 'attachment; filename=' + filename);
        res.setHeader('Content-type',assets.content_type);
        let remote = await axios.get(assets.url, {
            httpAgent: req.headers["user-agent"],
            headers: {
                "Accept": "application/octet-stream",
                "Authorization": `Bearer ${process.env.GITHUB_API_KEY}`,
            },
            responseType: "stream",
        })
        remote.data.pipe(res);
        */
    }
    catch (err) {
        console.error(err);
        return res.status(400).json({ success: false });
    }

})