import type { NextApiRequest, NextApiResponse } from 'next'
import { Octokit } from "@octokit/core";
import crypto from "crypto"
import {withAuth} from "../../../withSession";

/**
 * Checks if the current release is the latest release
 */

export default withAuth(async (req: NextApiRequest, res: NextApiResponse) => {

  if (req.method != "GET" || !req.query.version) {
    return res.status(400).json({ success: false });
  }
  const octokit = new Octokit({ auth: process.env.GITHUB_API_KEY });
  const config = {
    owner: process.env.GITHUB_OWNER,
    repo: process.env.GITHUB_REPO
  };
  try {
    let currentVer = req.query.version;
    if(currentVer >= "1.2.1") currentVer = "v"+currentVer; // Follows semantic versioning;
    const current = await octokit.request("/repos/{owner}/{repo}/releases/tags/{tag}", Object.assign({ "tag": currentVer }, config))
    const latest = await octokit.request("/repos/{owner}/{repo}/releases/latest", config);
    let data:any = {
      success: true,
      update: null
    }
    if (current.data.id !== latest.data.id) {
      // Requies an update, grab the asset ID and return the download token;
      data.update = {
        name: latest.data.name,
        date: latest.data.published_at,
        desc: latest.data.body,
        size: latest.data.assets[0].size,
        filename: latest.data.assets[0].name,
      }
      data.token = crypto.createHash("sha256").update(process.env.APK_STARTKEY + latest.data.assets[0].id + process.env.APK_ENDKEY).digest("hex")
    }
    return res.status(200).json(data);
  }
  catch (err) {
    console.log(err);
    return res.status(400).json({ success: false });
  }

})