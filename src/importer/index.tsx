import { Fragment, useEffect, Component, useState } from "react";
import Loader from "react-loader-spinner";
import { AlertCircle } from "react-feather";
import dayjs from "dayjs";
import SingletonRouter, { Router } from "next/router";
import { capitalize, sleep } from "../util";
import { Provider } from "../types";
import axios, { AxiosInstance, CancelTokenSource } from "axios";
import Image from "next/image";
import { Order, Item } from "../types";
import _ from "lodash";
import Modal from "../modal";
import NavigationPrompt from "../navigationPrompt";

type FetcherProps = {
  source: Provider;
  callback: Function;
};
type FetcherState = {
  show: boolean;
  error: any;
  text: string;
  cancel: boolean;
  warned: boolean;
};

const ProviderInfo = {
  foodpanda: {
    color: "text-foodpanda-500",
    bg: "bg-foodpanda",
    hex: "#D70F64",
    title: "Foodpanda",
  },
  lineman: {
    color: "text-lineman-500",
    bg: "bg-lineman",
    hex: "#2DBE57",
    title: "LINE MAN",
  },
};

export function Header({
  type,
  showBack = false,
  onBack = () => {},
}: {
  type: Provider;
  showBack: boolean;
  onBack?: Function;
}) {
  const { bg, title } = ProviderInfo[type];
  const fileExt = type == "foodpanda" ? ".webp" : ".png";
  let filename = "/" + type + fileExt;
  return (
    <header className={bg + " border rounded-lg p-5 text-white flex flex-row"}>
      <div className="flex-grow">
        <Image src={filename} width={64} height={64} className="" />
        <h1 className="font-bold md:text-3xl text-2xl px-3">{title} Import</h1>
      </div>
      {showBack && (
        <div>
          <button
            onClick={() => onBack()}
            className="bg-gray-100 border hover:text-blue-500 px-4 py-2 rounded focus:ring-blue-500 text-black"
          >
            Back
          </button>
        </div>
      )}
    </header>
  );
}

export class FetcherComponent extends Component<FetcherProps, FetcherState> {
  private apiInstance: AxiosInstance;
  private source: CancelTokenSource;
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      error: null,
      text: "loading",
      cancel: false,
      warned: false,
    };
    this.source = axios.CancelToken.source();
    this.apiInstance = axios.create({
      baseURL: process.env.NEXT_PUBLIC_API_ENDPOINT,
      timeout: 10000,
      cancelToken: this.source.token,
      params: {
        source: props.source,
      },
    });
  }

  cancel() {
    this.setState((state) => ({ ...state, cancel: true }));
    this.source.cancel();
    this.props.callback(false);
  }
  showWarning() {
    this.setState((state) => ({
      ...state,
      warned: true,
    }));
  }
  warned() {
    return this.state.warned;
  }
  setShow(show) {
    this.setState((state) => ({
      ...state,
      show: show,
      warned: show == true ? false : state.warned,
    }));
  }

  setText(text) {
    this.setState((state) => ({
      ...state,
      text,
    }));
  }

  setError({ at, desc, head }) {
    this.setState((state) => ({
      ...state,
      error: { at: this.props.source + "." + at, desc, head },
    }));
  }

  mailFetcher = async (resolve, reject, count) => {
    console.log("Starting fetcher - count:", count);
    try {
      let res = await this.apiInstance.get("/mail/listen");
      if (res.data.success) {
        resolve(res.data.data);
      } else if (count < 5) {
        count++;
        await sleep(4000);
        await new Promise(() => this.mailFetcher(resolve, reject, count));
      } else {
        reject(new Error("Cannot fetch mail"));
      }
    } catch (err) {
      reject(err);
    }
  };
  manualUpload = async (file) => {
    const fd = new FormData();
    fd.append("csv",file);
    try {
      let res = await this.apiInstance.post("/import/upload",fd, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      });
      if(res.data.success) {
        let attachId = res.data.data
        this.props.callback(attachId);
        this.setText("finalize");
      }
    }
    catch(err) {
      this.setError({
        at: "upload",
        head: "Cannot upload the current file.",
        desc: "Please check your console for further information.",
      });
    }
  }
  importData = async (data?) => {
    let downloadId;
    let attachId;
    if (this.props.source == "foodpanda") {
      // Foodpanda ONLY: Request file
      try {
        await this.apiInstance.post("/import/autofetch", data);
      } catch (err) {
        this.setError({
          at: "autofetch",
          head: "Cannot request the report automatically.",
          desc: "Please check your console for further information.",
        });
        return false;
      }
    }
    try {
      // Listen for mail changes
      this.setText("listening for mail changes");
      downloadId = await new Promise((r, j) => this.mailFetcher(r, j, 1));
      if (this.props.source !== "foodpanda") {
        this.setText("finalize");
        return this.props.callback(downloadId);
      }
    } catch (err) {
      this.setError({
        at: "listen",
        head: "Cannot fetch mailbox or no report mails are sent.",
        desc:
          "Recheck your mailbox. If the mail has been sent, consider using manual import.",
      });
      return false;
    }
    // Foodpanda ONLY: Download attachments
    this.setText("fetching reports");
    try {
      let res = await this.apiInstance.post(
        "/import/downloadAttach",
        new URLSearchParams({ id: downloadId })
      );
      attachId = res.data.data;
      this.setText("finalize");
      this.props.callback(attachId);
    } catch (err) {
      this.setError({
        at: "download",
        head: "Cannot get CSV reports or download expired.",
        desc:
          "Check your mailbox. If the mail has been sent, consider using manual import.",
      });
      return false;
    }
  };
  beforeunload(e) {
    // Full page unload; browser standard events

    const { show, error, text } = this.state;
    if (!show || (show && error !== null) || text == "finalize") {
      e.preventDefault();
      e.returnValue = "";
      return "beforeunload";
    }
  }
  componentDidMount() {
    // Router prevent navigation
    // See https://github.com/vercel/next.js/issues/2476#issuecomment-612483261
    // Warning: This overwrite private Router.change property.
    const { show, error, text } = this.state;
    // @ts-ignore: Router.change is private
    SingletonRouter.router.change = (...args) => {
      if (!show || (show && error !== null) || text == "finalize") {
      // @ts-ignore: Router.change is private
        return Router.prototype.change.apply(SingletonRouter.router, args);
      } else {
        this.showWarning();
        return new Promise((resolve, reject) => resolve(false));
      }
    };
    window.addEventListener("beforeunload", this.beforeunload);
  }

  componentWillUnmount = () => {
    // @ts-ignore: Router.change is private
    delete SingletonRouter.router.change;
    this.source.cancel();
    window.removeEventListener("beforeunload", this.beforeunload);
  };

  render() {
    const { error, text, show, warned } = this.state;
    const { color, hex } = ProviderInfo[this.props.source];
    if (!show) {
      return null;
    }
    return (
      <main className="p-5 py-10 flex flex-col flex-wrap items-center gap-4">
        {error === null ? (
          <Fragment>
            <Loader type="TailSpin" color={hex} height={80} width={80} />
            <span className="text-gray-700">{capitalize(text)}...</span>
            <button
              onClick={() => this.cancel()}
              className="px-4 py-2 rounded-sm bg-gray-200 hover:bg-gray-300"
            >
              Cancel
            </button>
            {warned && (
              <span className="text-red-500 font-medium">
                To navigate out of this page, please cancel first.
              </span>
            )}
          </Fragment>
        ) : (
          <Fragment>
            <AlertCircle color={hex} size={80} />
            <span className={color + " font-bold text-lg text-center"}>{error.head}</span>
            <span className="text-red-500 text-center text-sm">
              {error.desc} <br />
              (at {error.at})
            </span>
          </Fragment>
        )}
      </main>
    );
  }
}

export function FetcherTable(props) {
  const { parsed, error, raw } = props;
  if(parsed.length == 0) {
    return null;
  }
  const [_modal, setShow] = useState({
    show: false,
    index: 0,
    isError: null,
    source: true,
  });
  const currencyFormatter = new Intl.NumberFormat("th-TH", {
    style: "currency",
    currency: "THB",
  });
  const errorRows = [];

  const errorID = error.map((v) => v.id);
  const generateRows = (data: Order, i) => {
    let isError = errorID.indexOf(data.id);
    let badgeColor = isError == -1 ? "bg-green-600" : "bg-red-600";
    let row = (
      <tr
        key={i}
        className={
          "select-none border-b border-gray-200 hover:bg-gray-100 cursor-pointer" +
          (i % 2 != 0 ? " bg-gray-50" : "")
        }
        onClick={() => setShow({show: true, source: false, index: i, isError: (isError != -1 && error[isError].name)})}
      >
        <td className="py-3 px-2 break-all">{data.id}</td>
        <td className="py-3 px-2 break-words">
          {dayjs(data.timestamp).format("DD/MM/YYYY HH:mm:ss")}
        </td>
        <td className="py-3 px-2">
          {currencyFormatter.format(data.total)}
          <br />
          {data.count} item{data.count > 1 ? "s" : ""}
        </td>
        <td className="py-3 px-2">
          <span
            className={
              "text-white py-1 px-3 rounded-full text-xs " + badgeColor
            }
          >
            {isError === -1 ? "OK" : "Error"}
          </span>
          <br />
          <span className="text-sm text-red-500">
            {isError != -1 && error[isError].name}
          </span>
        </td>
      </tr>
    );
    if (!errorID.includes(data.id)) {
      return row;
    }
    errorRows.push(row);
  };
  const rows = parsed.map((d, i) => generateRows(d, i));
  return (
    <Fragment>
      <Modal show={_modal.show}>
        <div className="bg-white p-6 pb-4  border-b border-gray-200">
          <div className="flex items-start">
            <h3 className="flex-grow text-xl leading-6 font-medium text-gray-900">
              Order ID: {parsed[_modal.index].id}
            </h3>
            <span
            className={
              "ml-4 text-white py-1 px-3 rounded-full text-xs " + (!_modal.isError ? "bg-green-600" : "bg-red-600")
            }
          >
            {!_modal.isError ? "OK" : "Error"}
          </span>
          </div>
        </div>
        {_modal.source || _modal.isError ? (
          <div className="px-6 pt-5 sm:pt-6 max-h-64 overflow-y-scroll">
            {_modal.isError && (
              <h4 className="text-red-500 font-bold text-xl mb-4">{_modal.isError}</h4>
            )}
            {Object.entries(parsed[_modal.index].raw).map(([key,value],i) => (
              <pre key={i}>{key}: {value}</pre>
            ))}
          </div>
        ) : (
          <Fragment>
            <div className="bg-white px-4 pt-5 sm:pt-6 leading-9">
              <div className="flex flex-col items-start gap-y-4">
                {parsed[_modal.index].items.map(
                  (item: Item, index) => (
                    <Fragment key={index}>
                      <div className="grid w-full">
                        <div className="grid grid-cols-2 text-lg font-medium">
                          <h4>
                            {item.data.displayName}
                          </h4>
                          <span className="text-green-500 text-right">
                            {currencyFormatter.format(item.total)}
                          </span>
                        </div>
                        {item.sizes.map((size, s) => (
                          <div
                            key={s}
                            className="grid grid-cols-4 text-gray-500"
                          >
                            <span>{capitalize(size.type)}</span>
                            <span className="text-right">{size.price}</span>
                            <span>&nbsp; &times; {size.qty}</span>
                            <span className="text-green-500 text-right">
                              {currencyFormatter.format(size.qty * size.price)}
                            </span>
                          </div>
                        ))}
                      </div>
                    </Fragment>
                  )
                )}
              </div>
            </div>
            <div className="grid grid-cols-2 p-2 font-bold sm:px-6 m-2 border border-green-500 rounded-lg text-green-500">
              <span>Subtotal</span>
              <span className="text-right">
                {currencyFormatter.format(parsed[_modal.index].gpTotal)}
              </span>
            </div><div className="grid grid-cols-2 p-2 font-bold sm:px-6 mx-2 text-red-500">
              <span>GP fee</span>
              <span className="text-right">
                -{currencyFormatter.format(parsed[_modal.index].gpTotal - parsed[_modal.index].total)}
              </span>
            </div>
            <div className="grid grid-cols-3 p-2 mb-4 font-bold sm:px-6 m-2 bg-green-500 rounded-lg text-white">
              <span>Total</span>
              <span className="text-center">
                {parsed[_modal.index].count}{" "}
                {parsed[_modal.index].count > 1 ? "items" : "item"}
              </span>
              <span className="text-right">
                {currencyFormatter.format(parsed[_modal.index].total)}
              </span>
            </div>
          </Fragment>
        )}
        <div className="bg-gray-100 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
          <button
            type="button"
            onClick={() => setShow((state) => ({...state, show: false}))}
            className="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-blue-500 text-base font-medium text-white hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 sm:ml-3 sm:w-auto sm:text-sm"
          >
            OK
          </button>
          {!_modal.isError && (
          <div className="flex-grow sm:mt-1 mt-4 sm:text-left text-center ">
            <u
              className="text-gray-500 text-sm cursor-pointer"
              onClick={() =>
                setShow((state) => ({ ...state, source: !state.source }))
              }
            >
              [View {_modal.source ? "Parsed" : "Source"}]
            </u>
          </div>)}
        </div>
      </Modal>
      <table className="sm:p-5 my-4 w-full table-auto overflow-auto">
        <thead>
          <tr className="bg-gray-200 text-gray-600 uppercase text-sm leading-normal">
            <th className="py-3 px-4 md:px-6 w-16 sm:w-auto text-center">ID</th>
            <th className="py-3 px-4 md:px-6 w-8 sm:w-auto text-center">Date</th>
            <th className="py-3 px-4 md:px-6 w-8 sm:w-auto text-center">Total</th>
            <th className="sm:py-3 px-4 md:px-6 w-4 sm:w-auto text-center">Status</th>
          </tr>
        </thead>
        <tbody className="text-gray-600 text-sm md:text-base text-center">
          {errorRows}
          {rows}
        </tbody>
      </table>
    </Fragment>
  );
}

