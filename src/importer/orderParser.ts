import { promises as fs } from "fs"
import path from "path"
import { Menu, Provider } from "../types";
import csv from "neat-csv"
import _ from "lodash";
import { createLogger, format, transports } from 'winston';
const { combine, timestamp, label, prettyPrint, printf } = format;

const formatError = info => {
    if (info.message instanceof Error) {
      info.message = Object.assign({
        message: info.message.message,
        stack: info.message.stack
      }, info.message);
    }
  
    if (info instanceof Error) {
      return Object.assign({
        message: info.message,
        stack: info.stack
      }, info);
    }
  
    return info;
  }
  
const myFormat = printf(({ level, message, label, timestamp }) => {
  return `${timestamp} [${label}] ${level}: ${message}`;
});

const logger = createLogger({
  level: 'info',
  format: combine(
    timestamp(),
    label({ label: 'orderParser' }),
    prettyPrint(),
    myFormat
  ),
  transports: [
    new transports.File({ filename: path.join(process.cwd(),'server.log') }),
  ],
});


export default class OrderParser {
    menus: Menu[];
    menuGroup = {};
    source: Provider;
    fileError: boolean = false;
    filename: string;
    /**
     * Raw results parsed from PapaParse
     */
    rawResult = [];
    constructor(source: Provider) {
        this.source = source;
        logger.info("Starting orderParser for source: "+ source);
    }
    /**
     * @protected Local Filesystem file reader
     * @param {string} File name with extension
     * @param {boolean} Anonymous check. If true, read from anonymous dir.
     */
    protected async _readFile(filename: string, anonymous?) {
        logger.info("Reading file " +filename + (anonymous ? " (anonymous)" : ""));
        const source = anonymous ? "anonymous" : this.source;
        try {
            if(!this.menus) {
                const menus = await fs.readFile(path.join(process.cwd(), "menu.json"), { encoding: "utf-8" })
                this.menus = JSON.parse(menus);
                logger.info("Menu parse OK.")
            }
            const fullPath = path.join(process.cwd(), "mail", source, filename)
            let data = await fs.readFile(fullPath, {
                encoding: "utf-8"
            })
            if (data) {
                this.fileError = false;
                this.filename = filename.split('.').slice(0, -1).join('.');
                logger.info("File was able to read.")
                return data;
            }
            return null;
        }
        catch (err) {
            const errLog = formatError(err);
            logger.error(JSON.stringify(errLog));
            this.fileError = true;
        }
    }

    /**
     * Parse CSV with pre-filter
     * @param data CSV data
     * @returns Parse results
     */
    protected async parseCSV(data: string) {
        try {
            if (!data) return [];
            data = data.replace(/^\uFEFF/, ''); // Remove BOM
            const parser = await csv(data, {
                mapHeaders: ({ header, index }) => {
                    let splited = header.split(" ");
                    splited = splited.map((d: string, i) => {
                        if (i == 0) { return d.charAt(0).toLowerCase() + d.slice(1); }
                        return d.charAt(0).toUpperCase() + d.slice(1)
                    })
                    return splited.join("");
                }
            });
            logger.info("Parsed CSV for filename " + this.filename);
            return parser;
        }
        catch (err) {
            this.fileError = true;
            console.error(err);
        }
    }
    
    protected toFixed(i:number | string) {
        if(typeof i === "string") i = parseFloat(i);
        return parseFloat(i.toFixed(2));
    }

    queryMenuByName(name: string) {
        if(this.menuGroup[name]) return this.menuGroup[name];
        const query = this.menus.filter(v => v.alternativeName.includes(name) || v.displayName == name);
        if (query.length != 1) return false
        this.menuGroup[name] = query[0];
        return query[0];
    }

    async postFilter(data: any[]) {
        data = data.filter(v => v !== null).sort((a, b) => b.timestamp - a.timestamp);
        // Save parsed data to local cache;
        try {
            logger.info("Order Parsed successfully, writing cache file.");
            await fs.writeFile(path.join(process.cwd(), "mail", this.source, this.filename + "-parsed.json"),JSON.stringify(data));
            logger.info("Completed.");
            return data;
        }
        catch(err) {
            console.log(err);
            const errLog = formatError(err);
            logger.error(JSON.stringify(errLog));;
            return [];
        }

    }
}