import Imap, { ImapMessage } from "imap"
import { simpleParser }from "mailparser"
import { Stream } from "node:stream";
import fs from "fs";
import path from "path"
import { createLogger, format, transports } from 'winston';
const { combine, timestamp, label, prettyPrint, printf } = format;

const formatError = info => {
  if (info.message instanceof Error) {
    info.message = Object.assign({
      message: info.message.message,
      stack: info.message.stack
    }, info.message);
  }

  if (info instanceof Error) {
    return Object.assign({
      message: info.message,
      stack: info.stack
    }, info);
  }

  return info;
}

const myFormat = printf(({ level, message, label, timestamp }) => {
  return `${timestamp} [${label}] ${level}: ${message}`;
});

const logger = createLogger({
  level: 'info',
  format: combine(
    timestamp(),
    label({ label: 'mailParser' }),
    prettyPrint(),
    myFormat
  ),
  transports: [
    new transports.File({ filename: path.join(process.cwd(),'server.log') }),
  ],
});

export default class MailClient {
  public client: Imap;
  private box: Imap.Box;
  private rawMessages = [];
  public messages = [];
  public dir = "";
  constructor(debug: boolean = false) {
    const debugFn = debug ? (d) => console.log("MailParser:", d) : (d) => { 
      // Execute the local logger
      logger.info(d);
    } 
    try {
      this.client = new Imap({
        user: process.env.IMPORT_EMAIL,
        password: process.env.IMPORT_PASS,
        host: process.env.IMPORT_SERVER,
        port: 993,
        tls: true,
        tlsOptions: {
          rejectUnauthorized: false,
        },
        debug: debugFn,
        authTimeout: 7000
      });
      logger.info("----- " + (new Date).toISOString() + ": Start Request");
    }
    catch(err) {
      console.log(err);
      const errLog = formatError(err);
      logger.error(JSON.stringify(errLog));;
    }
  }
  getRecipents(source:string | string[], since?:string) {
    let filter = [];
    if(since !== undefined) {
      filter.push(["SINCE",since])
    }
    switch (source) {
      case "robinhood":
        filter.push(["FROM", "no-reply-robinhoodshop@robinhood.in.th"]);
        filter.push(["FROM", "edocument@mail.etaxsolution.com"]);
        break;
      case "foodpanda":
        //["FROM", "laps@scb.co.th"], 
        //filter = [["FROM", "noreply@portal.restaurant-partners.com"], ["SINCE", "Apr 9, 2021"]]
        filter.push(["FROM", "noreply@portal.restaurant-partners.com"]);
        break;
      case "lineman":
        filter.push(["FROM", "no-reply-merchant@lmwn.com"]);
        break;
      default:
        return [];
    }
    return filter
  }
  openInbox(): Promise<Imap.Box> {
    return new Promise((resolve, reject) => {
      this.client.openBox("INBOX", (err: Error, box: Imap.Box) => {
        if (err) reject(err);
        resolve(box);
        this.box = box;
      })
    })
  }
  fetchMail({ mode, pos, count }) {
    let startPos: number, endPos: number;
    switch (mode) {
      case "asc":
        startPos = pos;
        endPos = startPos + count - 1;
        break;
      case "desc":
        endPos = this.box.messages.total - pos + 1;
        startPos = endPos - count + 1;
    }
    let target = startPos + ":" + endPos;
    var f = this.client.seq.fetch(target, {
      bodies: ""
    });
    return f;
  }
  searchMail(search: any): Promise<Number[]> {
    return new Promise((resolve, reject) => {
      this.client.search(search, (err, res) => {
        if (err) reject(err);
        resolve(res);
      })
    })
  }
  /**
   * Read messages into a raw buffer and add to parsing queue.
   * @param msg {ImapMessage} Messages from fetch function
   * @param seqno {Number} Sequence number from fetch function
   */
   messageHandler(msg: ImapMessage, seqno: Number) {
    var prefix = '(#' + seqno + ') ';
    var stream;
    msg.on('body', (_stream: Stream, info) => {
      stream = _stream; //Copy whole thing
    });
    msg.once('end', () => {
      console.log(prefix + 'Finished');
      this.rawMessages.push({ id: seqno, data: stream });
    });
  }
  /*
  async getSourceFolder(source: string) {
    try {
      let pathname = path.join(process.cwd(), "mail",source);
      await fs.mkdir(pathname, { recursive: true });
      this.dir = pathname;
    }
    catch(err) {
      console.log(err);
    }
  }*/


  /**
   * Runs parser queue asynchronously using simpleParser
   * @param searchRes {Number[]} List of UIDs to replace sequence id.
   * @param source {string} Source Name for categorizing
   * @returns Post-process Array of messages
   */
  async parseQueue(searchRes?: Number[], source?: string) {
    return Promise.all(this.rawMessages.map(async (d, i) => {
      try {
        if (searchRes !== undefined) {
          d.id = searchRes[i];
        }
        let parsed = await simpleParser(d.data, {
          keepCidLinks: true
        });
        // Parse attachments
        if (parsed.attachments.length > 0) {
          let fi = 0;
          for (let file of parsed.attachments) {
            let filename = d.id + "_" + file.filename;
            if (source !== undefined) {
            fs.writeFileSync(path.join(process.cwd(), "mail",source, filename), file.content);
            parsed.attachments[fi].content = undefined;
            fi++;
          }
        }
      }

        // Our required headers already included in root
        parsed.headerLines = undefined;
        parsed.messageId = undefined;
        return { id: d.id, data: parsed }
      }
      catch (err) {
        console.error(err);
        throw err;
      }
    }))
  }
}
