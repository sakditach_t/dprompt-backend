/**
 * FOODPANDA Utils
 */

import axios from "axios";
import fs from "fs";
import dayjs from "dayjs";
import OrderParser from "./orderParser";
import { Order } from "../types";
import _ from "lodash"
import path from "path"
import { createLogger, format, transports } from 'winston';
const { combine, timestamp, label, prettyPrint, printf } = format;

const formatError = info => {
    if (info.message instanceof Error) {
      info.message = Object.assign({
        message: info.message.message,
        stack: info.message.stack
      }, info.message);
    }
  
    if (info instanceof Error) {
      return Object.assign({
        message: info.message,
        stack: info.stack
      }, info);
    }
  
    return info;
  }

const myFormat = printf(({ level, message, label, timestamp }) => {
  return `${timestamp} [${label}] ${level}: ${message}`;
});

const logger = createLogger({
  level: 'info',
  format: combine(
    timestamp(),
    label({ label: 'foodpandaFetcher' }),
    prettyPrint(),
    myFormat
  ),
  transports: [
    new transports.File({ filename: path.join(process.cwd(),'server.log') }),
  ],
});
export class FoodpandaFetcher {
    private user: string;
    private pass: string;
    private token: string;
    /**
     * Starts the Foodpanda Restaurant HTTP client and provides automation tasks.
     * 
     * The API endpoints has been reversed engineering from the web-portal.
     * It is not guarantee to run successfully as it might broken at any time.
     * @param user {string} Login username
     * @param pass {string} Login password
     */
    constructor(user: string, pass: string) {
        this.user = user;
        this.pass = pass;
        logger.info("Starting Foodpanda Client")
    }
    /**
     * Login using the provided credentials and exchange with an access token.
     * @returns Authentication status (true/false)
     */
    async login() {
        try {
            logger.info("Logging in..")
            let res = await axios.post("https://i2i8h1fo03.execute-api.ap-southeast-1.amazonaws.com/prd/v3/master/login", {
                email: this.user,
                password: this.pass
            },
                {
                    withCredentials: true
                });

            if (res.status == 401) {
                throw new Error("Invalid credentials");
            }
            
            logger.info("Logged in successfully..")
            this.token = res.data.accessToken;
            return true;
        }
        catch (err) {
            console.error(err);
            const errLog = formatError(err);
            logger.error(JSON.stringify(errLog));
            return false;
        }
    }
    /**
     * Request the CSV detailed order reports to the current email.
     * @param from Date string format (2021-03-05)
     * @param to Date string format (2021-03-08)
     * @returns Fetch status (true/false)
     */
    async getReports(from: string, to: string) {
        if (!this.token) { return false; }
        try {
            logger.info("Request report from " + from + " - "+ to);
            await axios.post("https://81ovwvv8hi.execute-api.ap-southeast-1.amazonaws.com/prd/v1/vendors/FP_TH;e021/reports/orders/details/export/share/email", {
                "format": "csv",
                "recipients": [this.user],
                "portalUrl": "https://foodpanda.portal.restaurant"
            }, {
                params: {
                    format: "csv",
                    from: from,
                    to: to,
                    locale: "en"
                },
                headers: {
                    "Authorization": "Bearer " + this.token
                }
            });
            return true;
        }
        catch (err) {
            const errLog = formatError(err);
            logger.error(JSON.stringify(errLog));;
            console.error(err);
            return false;
        }
    }
    /**
     * Download the requested CSV report and save to the local filesystem.
     * @param token Download token recieved from email
     * @param fileName Destination filename
     * @returns Download status (true/false)
     */
    async downloadReports(token: string, fileName: string) {
        const writer = fs.createWriteStream(fileName);
        try {
            logger.info("Download report using token " + token);
            let res = await axios.get("https://os-streaming-proxy.api.as.prd.portal.restaurant/v1/download/" + token, {
                withCredentials: true,
                headers: {
                    "Authorization": "Bearer " + this.token
                },
                responseType: "stream"
            })
            res.data.pipe(writer);
            return true;
        }
        catch (err) {
            const errLog = formatError(err);
            logger.error(JSON.stringify(errLog));;
            console.error(err);
        }
    }
    /**
     * (BETA) Log outs from the session using HTTP saved cookie (if included).
     * 
     * The endpoint is not guarantee to work. 
     * However, there's currently no risk from closing the client without loging out.
     * @returns Logout status
     */
    async logout() {
        try {
            let res = await axios.get("https://foodpanda.portal.restaurant/logout", {
                withCredentials: true
            });
            logger.info("Logout", res.status);
            return res.data;
        }
        catch (err) {
            console.error(err);
            const errLog = formatError(err);
            logger.error(JSON.stringify(errLog));;
            return false;
        }
    }
}

export class FoodpandaParser extends OrderParser {
    results: Order;
    errors = [];
    constructor() {
        super("foodpanda");
    }
    async readFile(filename) {
        try {
            let file = await this._readFile(filename + ".csv");
            if(!file) {
                file = await this._readFile(filename + ".csv" ,true);
            }
            this.rawResult = await this.parseCSV(file);
            return;
        }
        catch(err) {
            console.log(err);
        }
    }
    async constructOrder() {
        const orders = this.rawResult.map((v,i) => {
            if (!v.orderID || v.billableStatus != "yes") {
                return null;
            }
            const { items, count, error, _total} = this.parseItems(v.items);
            if(error) {
                this.errors.push({
                    id: "FPD-" + v.orderID,
                    name: "Parsing item error",
                    raw: v,
                    parsed: items
                })
            }
            else if(_total != parseInt(v.foodValue)) {
                this.errors.push({
                    id: "FPD-" + v.orderID,
                    name: "Price not match",
                    raw: v,
                });
            }
            const gpFee = parseInt(v.foodValue) * 0.32;
            const gpFeeVAT = gpFee * 0.07
            return {
                source: "foodpanda",
                count,
                id: "FPD-" + v.orderID,
                items,
                error,
                timestamp: dayjs(v.acceptedAt).toDate().valueOf(),
                gpTotal: parseInt(v.foodValue),
                total: this.toFixed(parseInt(v.foodValue) - gpFee - gpFeeVAT),
                raw: v
            };

        }).filter(v => v !== null);
        return await this.postFilter(orders);
    }

    parseItems(str: string) {
        let error = true;
        let count = 0;
        let items = str.split(", ").map(v => {
            const size = v.substring(v.lastIndexOf(" ") + 1, v.length - 1);
            v = v.substring(0, v.lastIndexOf(" ") + 1).trim();
            const qty = parseInt(v.substring(0,1));
            const name = v.substring(2, v.length-3);
            let price = null, fid = name;
            const type = (size == "ธรรมดา" || size == "Regular") ? "normal" : "extra";
            const menu = this.queryMenuByName(name);
            if(menu) {
                fid = menu.id;
                price = type == "normal" ? menu.delivery.price : menu.delivery.extraPrice;
                error = false;
            }
            return {
                fid,
                data: menu,
                sizes: [{
                    type,
                    qty,
                    price
                }]
            }
        });
        let _total = 0; // Global Items total
        let group = _.chain(items).groupBy("fid").mapValues((m) => {
            let _subtotal = 0; // Food subtotal
            let sizes = _.chain(m).map("sizes").flattenDeep().map( v => {
                count = count + v.qty;
                let total = v.price * v.qty;
                _subtotal = _subtotal + total;
                return {...v, total}
            }).value(); 
            _total = _subtotal + _total
            return {fid: null,data: m[0].data,sizes,total: _subtotal};
        }).value();
        let mapper = Object.entries(group).map((v) => {
            v[1].fid = v[0];
            return v[1];
        });
        return { count, items: mapper, error, _total}
    }
}