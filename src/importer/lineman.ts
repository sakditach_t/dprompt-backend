/**
 * LINEMAN Utils
 */

import OrderParser from "./orderParser";
import { Order } from "../types";
import _ from "lodash"
import { ParsedMail } from "mailparser";
import dayjs from "dayjs"
import customParseFormat from "dayjs/plugin/customParseFormat";
export class LineMANParser extends OrderParser {
    results: Order;
    errors = [];
    constructor() {
        super("lineman");
        dayjs.extend(customParseFormat)
    }
    async readFile(filename) {
        try {
            const file = await this._readFile(filename + ".json");
            if (file) {
                let db = JSON.parse(file);
                for (const data of db) {
                    const v: ParsedMail = data.data
                    for (let attach of v.attachments) {
                        if (attach.filename == "report.csv") {
                            // Try to read and parse the attachment's content.
                            let csv = await this._readFile(data.id + "_" + v.attachments[0].filename);
                            let parser = await this.parseCSV(csv);
                            if (parser.length > 0) {
                                // At the end; use filename from query;
                                this.filename = filename;
                                this.rawResult = parser;
                                return;
                            }
                        }
                    }
                }

            }
        }
        catch (err) {
            console.log(err);
        }
        // Use direct file reading
        this.rawResult = await this.parseCSV(await this._readFile(filename + ".csv", true));
    }
    async constructOrder() {
        let orders = this.rawResult.map((v, i) => {
            if (!v.orderID || v.orderStatus != "FINISH") {
                return null;
            }
            let { items, count, error } = this.parseItems(v.itemName);
            if (error) {
                this.errors.push({
                    id: v.orderID,
                    name: "Parsing item error",
                    raw: v,
                    parsed: items
                })
            }
            //We can't use totalPrice, it's crazy.
            const total = this.toFixed(parseInt(v.revenueAmount) - parseFloat(v.gPFee) - parseFloat(v.gPFeeVat));
            return this.getPrice({
                source: "lineman",
                count,
                id: v.orderID,
                items,
                error,
                timestamp: dayjs(v.orderTime).valueOf(),
                gpTotal: parseInt(v.revenueAmount),
                total,
                raw: v
            });

        }).filter(v => v !== null);
        return await this.postFilter(orders);
    }

    parseItems(str: string) {
        let error = false;
        let count = 0;
        let items = str.split("|").map(v => {
            const qty = parseInt(v.substring(v.lastIndexOf(" ") + 1, v.length));
            const name = v.substring(0, v.lastIndexOf(" "))
            if (name == "ไม่รับช้อนส้อมพลาสติก") return false
            count = count + qty;
            let fid = name;
            const menu = this.queryMenuByName(name);
            if (menu) {
                fid = menu.id;
            }
            if (!menu) {
                error = true;
            }
            return {
                fid,
                data: menu,
                sizes: [{
                    qty,
                }]
            }
        }).filter(v => v !== false);
        let _total = 0;
        let group = _.chain(items).groupBy("fid").value();
        let mapper = Object.values(group).map(v => v[0]);
        return { count, items: mapper, error, _total }
    }
    // Wongnai RMS doesn't report price, how bad is it.
    getPrice(data) {
        if (data.error) return data;
        if (data.count == 1) {
            // Easy: 1 Item, 1 qty return their prices immediately
            data.items[0].total = data.gpTotal;
            let size = (data.gpTotal == data.items[0].data.delivery.price) ? "normal" : "extra";
            data.items[0].sizes[0] = {
                qty: 1,
                price: data.gpTotal,
                type: size
            };
        }
        else if (data.items.length == 1) {
            data.items[0].total = data.gpTotal;
            data.items[0].sizes = data.items[0].sizes.map(v => {
                let baseItem = data.items[0].data
                const price = data.gpTotal / v.qty
                return {
                    ...v,
                    price,
                    type: (baseItem.delivery.price == price) ? "normal" : "extra"
                }
            })
        }
        else {
            // Start Math algorithm
            let prices = data.items.map(v => ([v.sizes[0].qty * v.data.delivery.price, v.sizes[0].qty * v.data.delivery.extraPrice]));
            let priceIndex = this.generatePossibleIndex(data.items.length);
            let check = true;
            for (let index of priceIndex) {
                if (check) {
                    let _total = 0;
                    let price = index.map((v, i) => {
                        _total = _total + prices[i][v];
                        return prices[i][v];
                    });
                    if (data.gpTotal == _total) {
                        // Match. Update price
                        price.map((v, i) => {
                            data.items[i].sizes[0].price = v / data.items[i].sizes[0].qty;
                            data.items[i].sizes[0].type = (index[i] == 0) ? "normal" : "extra";
                            data.items[i].total = v;
                        })
                        check = false;
                    }
                }

            }
        }
        return data;
    }
    /**
     * Helper function used by Math Algorithm.
     * 
     * Generates an array with possible price indexes for current item.
     */
    private generatePossibleIndex(length) {
        function containsArr(arr, search) {
            return !!arr.map(v => _.isEqual(v, search)).filter(v => v)[0];
        }
        let keyRand = [new Array(length).fill(0), new Array(length).fill(1)];
        let loop = true;
        while (loop) {
            const shuffle = _.shuffle(_.fill(new Array(length).fill(0), 1, Math.floor(Math.random() * length)));
            if (!containsArr(keyRand, shuffle)) {
                keyRand.push(shuffle);
            }
            if (keyRand.length == Math.pow(2, length)) {
                loop = false;
            }
        }
        return keyRand;
    }
}