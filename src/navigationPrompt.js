/**
 * Navigation Prompt Hook with onbeforeunload
 * https://github.com/vercel/next.js/issues/2476#issuecomment-741461010
 */


import {useEffect} from 'react'
import SingletonRouter, {Router} from 'next/router'

const NavigationPrompt = ({when, message}) => {
    function beforeunload(e) {
        if(when) {
            e.preventDefault();
            e.returnValue = "";
            return "beforeunload";
        }
    }
    useEffect(() => {
        SingletonRouter.router.change = (...args) => {
            if(!when) return Router.prototype.change.apply(SingletonRouter.router, args);
            return confirm(message)
                ? Router.prototype.change.apply(SingletonRouter.router, args)
                : new Promise((resolve, reject) => resolve(false));
        }
        
        window.addEventListener("beforeunload", beforeunload);
        return () => {
            delete SingletonRouter.router.change;
            window.removeEventListener("beforeunload", beforeunload);
        }
    }, [])

    return null;
}

export default NavigationPrompt;