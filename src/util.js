
import path from "path";
import dayjs from "dayjs";

export const redirectHeader = () => {
    return {
      redirect: {
        destination: "/import/error?type=unauthorized",
      },
    };
  }
  
  export const checkAuthCookie = async (auth, token) => {
    // Check if token exists
    if (!token || !context.params.id) {
      return false;
    }
    // Verify token ID
    try {
      let decodedToken = await auth.verifyIdToken(token);
      if (!decodedToken || !decodedToken.uid) throw Error("Not authenticated");
      return true;
    } catch (err) {
      console.error(err);
      return false;
    }
  }
  export const capitalize = (str) => {
    if (typeof str != "string") {
      return;
    }
    return str.charAt(0).toUpperCase() + str.slice(1);
  };
  export function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
export async function getMenuLastModified(fs) {
  const stats = await fs.stat(path.join(process.cwd(), "menu.json"));
  return dayjs(stats.mtime).format("DD/MM/YYYY HH:mm:ss");
}