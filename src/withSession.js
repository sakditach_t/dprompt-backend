import admin from "./firebase-admin";
import nookies from "nookies";

export function withAuth(handler) {
  return async (req, res) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      return res.status(401).json({ success: false });
    }
    const auth = admin.auth();
    const token = authHeader.split(" ")[1];
    let decodedToken;
    try {
      decodedToken = await auth.verifyIdToken(token);
      if (!decodedToken || !decodedToken.uid)
        return res.status(401).json({ success: false });
    } catch (error) {
      console.log(error);
      const status = !isNaN(error.status) ? parseInt(error.status) : 401;
      return res.status(status).json({
        success: false,
      });
    }

    return handler(req, res);
  };
}

export function withCookie(handler) {
  return async (req, res) => {
    const cookies = nookies.get({req});
    // Check if token exists
    try {
      await admin.auth().verifySessionCookie(cookies._auth, true);
      return handler(req, res);
    } catch (err) {
      return res.status(401).json({ success: false });
    }
  };
}
